class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.integer :itm_id
      t.string :itm_name
      t.binary :itm_photo
      t.integer :itmtyp_id

      t.timestamps null: false
    end
    add_index :items, :itm_id
  end
end
